const path = require('path')
const config = require('./webpack.config.js')

config.devServer = {
  historyApiFallback: true,
  static: path.join(__dirname, '../build'),
  port: 8080
}

config.devtool = 'inline-source-map'

module.exports = config

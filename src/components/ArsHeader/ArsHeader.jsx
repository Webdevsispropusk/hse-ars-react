import React, {useState} from 'react'
import {
  Header,
  HeaderHamburger,
  Space,
  Switcher,
  HeaderIcon,
  HeaderLink,
  LogoDigital,
  HeaderMenu,
  HeaderLanguageSelect,
  HeaderDivider,
  IconGlobalMessage,
  IconActionLogout,
  Size,
  HeaderUserAvatar
} from "@hse-design/react";

function ArsHeader() {
  const [currentLanguage, setCurrentLanguage] = useState("ru");
  const [isAuth, setIsAuth] = useState(false);
  const [title] = useState("Сервис размещения студентов (вне общежитий)");
  const userImg =
    "https://upload.wikimedia.org/wikipedia/commons/7/7c/Profile_avatar_placeholder_large.png";
  const languages = [
    {
      label: "RU",
      value: "ru",
    },
    {
      label: "EN",
      value: "en",
    },
  ];
  const menu = [
    {
      name: {
        ru: "Заявки",
        en: "Applications",
      },
      page: "Applications",
    },
    {
      name: {
        ru: "Сервисы",
        en: "Services",
      },
      page: "Services",
    },
    {
      name: {
        ru: "Задачи",
        en: "Tasks",
      },
      page: "Tasks",
    },
    {
      name: {
        ru: "Вопросы и ответы",
        en: "Q&A",
      },
      page: "QA",
    },
  ];
  return (
    <>
      <Header
        prepend={
          <div
            className={"tablet-small-down-show"}
            style={{ display: "flex", alignItems: "center" }}
          >
            <HeaderHamburger />
            <Space
              className="tablet-small-only-show"
              horizontal
              size={Space.Size.small_3x}
            />
          </div>
        }
        logo={<LogoDigital style={{ width: "100px" }} />}
        left={
          isAuth ? (
            <HeaderMenu>
              {menu.map(({ name, page }) => (
                <HeaderLink key={page}>{name[currentLanguage]}</HeaderLink>
              ))}
            </HeaderMenu>
          ) : (
            title
          )
        }
        right={
          <>
            <div
              className="tablet-small-down-hide"
              style={{ display: "flex", alignItems: "center" }}
            >
              <HeaderLanguageSelect
                languages={languages}
                currentLanguage={currentLanguage}
                onChange={setCurrentLanguage}
              />
              <HeaderDivider />
            </div>
            <HeaderIcon icon={IconGlobalMessage} />
            <HeaderDivider className="tablet-small-down-hide" />
            {!isAuth ? (
              <HeaderMenu className="tablet-small-down-hide">
                <HeaderLink>Войти</HeaderLink>
                <HeaderLink>Регистрация</HeaderLink>
              </HeaderMenu>
            ) : (
              <>
                <HeaderIcon
                  className="tablet-small-down-hide"
                  icon={IconActionLogout}
                />
                <Space size={Size["small_2x"]} horizontal />
                <HeaderUserAvatar userImg={userImg} />
              </>
            )}
            <>
              <HeaderDivider className="table-small-down-show" />
              <HeaderHamburger />
            </>
          </>
        }
      />
      <Space size="small" vertical />
      {/* <Switcher
        checked={isAuth}
        onChange={setIsAuth}
        label={`${isAuth ? "Авторизованный" : "Неавторизованный"} пользователь`}
      /> */}
    </>
  )
}

export default ArsHeader

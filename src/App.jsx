import React, { useState } from "react";
import ArsHeader from './components/ArsHeader/ArsHeader.jsx'
// import { BrowserRouter as Router, Switch } from 'react-router-dom';
// import { cn } from './service/classname';
// import { PageLayout } from '@hse-design/react';
import "./App.css";
import ArsFooter from "./components/ArsFooter/ArsFooter.jsx";

function App() {

  return (
    <>
      <div className="App">
        <ArsHeader />
        <main>

        </main>
        <ArsFooter />
      </div>
    </>
  );
}

export default App;
